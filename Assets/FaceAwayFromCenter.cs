﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceAwayFromCenter : MonoBehaviour
{
    [InspectorName("CenterOfPlanets")]
    public Transform target;

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = transform.position - target.position;
        transform.up = direction;
    }
}
