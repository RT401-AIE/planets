﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    public Transform LookTransform;
    public Animator animator;
    public Camera cam;
    public GameObject player;

    public float speed;
    public float maxVelocityChange;
    public float jumpForce;
    public float GroundHeight;

    public bool isGrounded;

    private void Start()
    {
        animator = FindObjectOfType<Animator>();
        cam = FindObjectOfType<Camera>();
    }

    private void Update()
    {
        float fwd = Input.GetAxis("Vertical");
        if (player != null)
        {
            animator.SetFloat("Foward", fwd);
            animator.SetFloat("Sense", Mathf.Sign(fwd));
            animator.SetFloat("Turn", Input.GetAxis("Horizontal"));
        }
        else
        {
            animator.SetFloat("Foward", Mathf.Abs(fwd));
            animator.SetFloat("Sense", Mathf.Abs(fwd));
            animator.SetFloat("Turn", Input.GetAxis("Horizontal"));
        }
    }

    public Vector3 foward;
    public Vector3 right;

    void FixedUpdate()
    {
        RaycastHit groundedHit;
        isGrounded = Physics.Raycast(transform.position, -transform.up, out groundedHit, GroundHeight);

        if (isGrounded)
        {
            // Calculate how fast we should be moving
            foward = Vector3.Cross(transform.up, -LookTransform.right).normalized;
            right = Vector3.Cross(transform.up, LookTransform.forward).normalized;
            Vector3 targetVelocity = (foward * Input.GetAxis("Vertical") + right * Input.GetAxis("Horizontal")) * speed;

            Vector3 velocity = transform.InverseTransformDirection(GetComponent<Rigidbody>().velocity);
            velocity.y = 0;
            velocity = transform.TransformDirection(velocity);
            Vector3 velocityChange = transform.InverseTransformDirection(targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;
            velocityChange = transform.TransformDirection(velocityChange);

            GetComponent<Rigidbody>().AddForce(velocityChange, ForceMode.VelocityChange);

            if (Input.GetButton("Jump"))
            {
                GetComponent<Rigidbody>().AddForce(transform.up * jumpForce, ForceMode.VelocityChange);
            }
        }
    }
}
