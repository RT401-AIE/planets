﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    [Range(0, 360)]
    public float RotationVar;
    public Transform target;

    public bool PlayerControled;

    void Update()
    {
        if (PlayerControled == false)
        {
            transform.LookAt(target);
            transform.Translate((Vector3.right * RotationVar) * Time.deltaTime);
        }

        if(PlayerControled == true)
        {
            transform.LookAt(target);
            if(Input.GetKey(KeyCode.D))
                transform.Translate((Vector3.right * RotationVar) * Time.deltaTime);

            if (Input.GetKey(KeyCode.A))
                transform.Translate((Vector3.right * -RotationVar) * Time.deltaTime);

            if (Input.GetKey(KeyCode.W))
                transform.Translate((Vector3.up * RotationVar) * Time.deltaTime);

            if (Input.GetKey(KeyCode.S))
                transform.Translate((Vector3.up * -RotationVar) * Time.deltaTime);
        }
    }
}
