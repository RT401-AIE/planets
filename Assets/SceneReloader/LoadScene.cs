﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public string scene;

    public void LoadLevel()
    {
        // load the levels and resets the scoreboard currents
        SceneManager.LoadScene(scene);
    }

}
