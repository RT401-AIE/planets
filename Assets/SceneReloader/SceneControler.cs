﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneControler : MonoBehaviour
{
    public string scene;
    public string Menu;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ReloadMenu();
        }
    }

    public void Start()
    {
        // sets the main menu scene
        Menu = "MainMenu";
    }

    public void ReloadMenu()
    {
        // loads the main menu scene
        SceneManager.LoadScene(Menu);
    }
}
